import dk.itu.model._
import dk.itu.extractor.GithubExtractor._
import spray.json._
import DefaultJsonProtocol._
import com.github.tototoshi.csv._
import better.files._
object Main {
	def main(args: Array[String]): Unit = {

		if(args.size != 2){
			println("Program needs two arguments. Name and CSV or JSON or BOTH")
			System.exit(1)
		}

		val reponame = args(0)
		val outputType = args(1)
		val forks = getProjectForks(reponame)

		def writeToCSV = {
			val header = "id,full_name,owner_login,fork,created_at,updated_at,pushed_at,stargazers_count,language,forks_count,default_branch,parent,subscribers_count\n"
				val filename = reponame.replaceAll("/","_")+".csv"
				val file = File(filename)
				file.write(header)
				// file.close
				val writer = CSVWriter.open(new java.io.File(filename), append=true)
				forks.foreach(fork => {
					writer.writeRow(fork.toString.split(","))
				})
		}

		def writeToJSON = {
			val file = File(reponame.replaceAll("/","_")+".json")
			file.write(forks.toJson.prettyPrint)
		}

		outputType match {
			case "CSV" => writeToCSV
			case "JSON" => writeToJSON
			case "BOTH" => {
				writeToCSV
				writeToJSON
			}
		}
	}

}