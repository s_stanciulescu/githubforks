package dk.itu.model

/* Copyright (c) 2016-2017 Stefan Stanciulescu
   IT University of Copenhagen

    version 0.1

    This file is part of GithubForksView.
    GithubForksView is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GithubForksView is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GithubForksView.  If not, see <http://www.gnu.org/licenses/>.

 */

import spray.json.DefaultJsonProtocol._
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormatter, ISODateTimeFormat}
import spray.json.{DefaultJsonProtocol, DeserializationException, JsString, JsValue, RootJsonFormat}

/**
  * Created by scas on 01-07-2016.
  */
object Model {

  implicit object DateJsonFormat extends RootJsonFormat[DateTime] {

    private val parserISO : DateTimeFormatter = ISODateTimeFormat.dateTimeNoMillis();

    override def write(obj: DateTime) = JsString(parserISO.print(obj))

    override def read(json: JsValue) : DateTime = json match {
      case JsString(s) => parserISO.parseDateTime(s)
      case _ => throw new DeserializationException("Error info you want here ...")
    }
  }


//  object DateTimeISO8601CodecJsons {
//
//    implicit def DateTimeAsISO8601EncodeJson: EncodeJson[DateTime] =
//      EncodeJson(s => jString(s.toString))
//
//    implicit def DateTimeAsISO8601DecodeJson: DecodeJson[DateTime] =
//      implicitly[DecodeJson[String]].map(org.joda.time.DateTime.parse(_)) setName "org.joda.time.DateTime"
//  }

//  import DateTimeISO8601CodecJsons._

  final case class GithubURL(username: String, reponame: String, branch: String, filename: String)


//  final case class RepoMain(id: Int, full_name: String, fork: Boolean, created_at: org.joda.time.DateTime, updated_at: org.joda.time.DateTime, pushed_at: org.joda.time.DateTime, language: Option[String], forks_count: Int,default_branch: String)

//  implicit val repoMain = jsonFormat9(RepoMain)

  case class Parent(full_name: String)
  implicit val parentCodec = jsonFormat1(Parent)

  case class Owner(login: String)
  implicit val ownerCodec = jsonFormat1(Owner)

  final case class GithubRepository(id: Int, full_name: String, owner: Owner, fork: Boolean, created_at: org.joda.time.DateTime, updated_at: org.joda.time.DateTime, pushed_at: org.joda.time.DateTime, 
    stargazers_count: Int, language: Option[String], forks_count: Int,default_branch: String, parent: Option[Parent], subscribers_count: Option[Int]){
    override def toString = {
      val lang = language match {
        case Some(x) => x
        case None => ""
      }
      val par = parent match {
        case Some(x) => x.full_name
        case None => ""
      }
      val subs = subscribers_count.getOrElse(0)
      s"""$id,$full_name,${owner.login},$fork,$created_at,$updated_at,$pushed_at,$stargazers_count,$lang,$forks_count,$default_branch,$par,$subs"""
    }
    } 

    // subscribers do not appear when accessing the /forks api
  implicit val repositoryCodec = jsonFormat13(GithubRepository)

//  final case class RepoFork(id: Int, full_name: String, fork: Boolean, created_at: org.joda.time.DateTime, updated_at: org.joda.time.DateTime, pushed_at: org.joda.time.DateTime, language: String, forks_count: Int,default_branch: String, parent: Option[Parent])

//  implicit val repoForkCodec = jsonFormat10(RepoFork)

//  final case class Repository(id: Int, full_name: String, owner: Owner, forks_url: String, created_at: org.joda.time.DateTime, updated_at: org.joda.time.DateTime, pushed_at: org.joda.time.DateTime, forks_count: Int, parent: Parent)
//  implicit val repositoryCodec = jsonFormat9(Repository)

//  case class MainRepository(id: Int, full_name: String, owner: Owner, forks_url: String, created_at: org.joda.time.DateTime, updated_at: org.joda.time.DateTime, pushed_at: org.joda.time.DateTime, forks_count: Int)
//  implicit val mainRepositoryCodec = jsonFormat8(MainRepository)

  final case class AuthorGithub(name: String, email: String, date: String)
  implicit val authorCodec = jsonFormat3(AuthorGithub)

  final case class CommitterGithub(name: String, email: String, date: String)
  implicit val committerCodec = jsonFormat3(CommitterGithub)

  final case class CommitMetadata(author: AuthorGithub, committer: CommitterGithub, message: String)
  implicit val commitMetadataCodec = jsonFormat3(CommitMetadata)

  final case class CommitParent(sha: String)
  implicit val commitParentCodec = jsonFormat1(CommitParent)

  final case class CommitGithub(sha: String, commit: CommitMetadata, parents: Option[Seq[CommitParent]])
    implicit val commitGithubFormat = jsonFormat3(CommitGithub)

  final case class Core(remaining: Int, reset: Long)
  implicit val coreCodec = jsonFormat2(Core)

  final case class Resource(core: Core)
  implicit val resourceCodec = jsonFormat1(Resource)

  final case class Resources(resources: Resource)
  implicit val resourcesCodec = jsonFormat1(Resources)

  final case class User(login: String, id: Int, name: Option[String], email: Option[String], created_at: org.joda.time.DateTime)
  implicit val userCodec = jsonFormat5(User)

  final case class UserNoEmail(login: String, id: Int, name: String, created_at: org.joda.time.DateTime)
  implicit val usernoemailCodec = jsonFormat4(UserNoEmail)

  final case class UserNoName(login: String, id: Int, created_at: org.joda.time.DateTime)
  implicit val userNoNameCodec = jsonFormat3(UserNoName)

  final case class SimpleUser(login: String, id: Int)
  implicit val simpleUserCodec = jsonFormat2(SimpleUser)

  final case class BranchCommit(sha: String, url: String)
  implicit val branchCommitCodec = jsonFormat2(BranchCommit)

  final case class Branch(name: String, commit: BranchCommit)
  implicit val branchCodec = jsonFormat2(Branch)

  final case class IssuePullRequest(url: String)
  implicit val issuePullRequest = jsonFormat1(IssuePullRequest)

  final case class Issues(id: Int, number: Int, title: String, user: SimpleUser, state: String, comments: Int, created_at: org.joda.time.DateTime, updated_at: Option[String], closed_at: Option[String], pull_request: Option[IssuePullRequest], body: Option[String])
  implicit val issuesCodec = jsonFormat11(Issues)

  final case class SimpleRepository(id: Long, full_name: String)
  implicit val simpleRepositoryCodec = jsonFormat2(SimpleRepository)

  final case class Head(user: SimpleUser, repo: Option[SimpleRepository])
  implicit val headCodec = jsonFormat2(Head)

  final case class Base(user: SimpleUser)
  implicit val baseCodec = jsonFormat1(Base)

  final case class SinglePullRequest(id: Long, patch_url: String, number: Long, state: String, title: String, user: Option[SimpleUser], body: Option[String], created_at: org.joda.time.DateTime, updated_at: Option[String], closed_at: Option[String], head: Option[Head], merged_at: Option[String], merge_commit_sha: Option[String], merged: Boolean, mergeable: Option[Boolean], merged_by: Option[SimpleUser], comments: Option[Long], commits: Option[Long], additions: Option[Long], deletions: Option[Long], changed_files: Option[Long])
  implicit val singlePullRequest = jsonFormat21(SinglePullRequest)

  final case class PullRequests(id: Long, number: Long, state: String, title: String, user: Option[SimpleUser], body: String, created_at: org.joda.time.DateTime, updated_at: Option[String], closed_at: Option[String], merged_at: Option[String], merge_commit_sha: Option[String], head: Option[Head], base: Option[Base])
  implicit val pullRequestsCodec = jsonFormat13(PullRequests)

  // There are two types of comments - commit comments and issue comments. The commit comments have a commit-id.
  final case class Comments(html_url: String, id: Long, user: SimpleUser, commit_id: Option[String], created_at: org.joda.time.DateTime, updated_at: Option[String], body: Option[String])
  implicit val commentsCodec = jsonFormat7(Comments)

}
